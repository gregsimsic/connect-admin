# CRF Virtual Meeting CMS

This is the Craft CMS backend content manager for CRF's virtual meetings portal. It provides a GraphQL endpoint for consumption by a NextJs app.

## Requirements

The site was developed locally on PHP 7.4 and MariaDb 10.5.8.

Craft CMS 3.5 minimum requirements are PHP 7.0 and MySql 5.5 [(complete Craft requirements)](https://craftcms.com/docs/3.x/requirements.html#minimum-system-specs)


## Installation

1. Setup a local domain and database

1. `git clone git@bitbucket.org:gregsimsic/connect-admin.git`

1. Copy .env.example to .env
   * Insert database connection settings
   * Set PRIMARY_SITE_URL to your local domain  
    
1. Run `composer install`

1. Populate the database. Perform one of these:

    A. Recommended: Export production database and import into local database to get all production data
    
    B. Or, to set up a clean site without data, skip this step and complete the next step to let Craft run the necessary updates
    
    C. Or, to set up a clean site without data manually, run `./craft ... apply config`
    
1. Navigate to https://<your-local-domain>/admin to login

## Notes

There are no frontend pages to this app. You can navigate to https://<your-local-domain>/ to see a simple 'home' screen.

### Local Image Assets
Media files are not tracked in this repo. If you want to work with the images locally, you will need to copy/sync the contents of the production /web/media folder into the same folder in your local site.

## Update your local site

...to come

## Deploy

...to come


## Craft Commands

`./craft project-config/apply`

