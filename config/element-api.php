<?php

use craft\elements\Entry;

return [
    'endpoints' => [

        'navigation' => function() {
            return [
                'elementType' => Entry::class,
                'criteria' => [
                    'section' => 'navigation',
                    'limit' => null
                ],
                'paginate' => false,
                'cache' => 'PT1M', // 'PT1M' one minute
                'transformer' => function(Entry $entry) {

                    $parent = $entry->getParent();

                    return [
                        'title' => $entry->title,
                        'id' => $entry->id,
                        'slug' => $entry->slug,
                        'type' => $entry->type->handle,
                        'parent' => $parent ? $parent->id : null
                    ];
                },
            ];
        },

        'html/<id:\d+>' => function($id) {
            return [
                'elementType' => Entry::class,
                'criteria' => [
                    'section' => 'htmlPages',
                    'id' => $id
                ],
                'one' => true,
                'cache' => false, // 'PT1M' one minute
                'transformer' => function(Entry $entry) {

                    return [
                        'title' => $entry->title,
                        'id' => $entry->id,
                        'slug' => $entry->slug
                    ];
                },
            ];
        }
    ]
];