<?php
/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here. You can see a
 * list of the available settings in vendor/craftcms/cms/src/config/GeneralConfig.php.
 *
 * @see \craft\config\GeneralConfig
 */

use craft\helpers\App;

define('BASEPATH', realpath(CRAFT_BASE_PATH) . '/');

return [
    // Global settings
    '*' => [
        // Default Week Start Day (0 = Sunday, 1 = Monday...)
        'defaultWeekStartDay' => 1,

        // Whether generated URLs should omit "index.php"
        'omitScriptNameInUrls' => true,

        // Control panel trigger word
        'cpTrigger' => 'admin',

        // The secure key Craft will use for hashing and encrypting data
        'securityKey' => App::env('SECURITY_KEY'),

        'enableGraphQlCaching' => true,

        // Origins that should be allowed to access the GraphQL API
        'allowedGraphqlOrigins' => [
            'https://connect.treesandbuildings.com/',
            'https://connect-fellows.treesandbuildings.com/'
        ],

        'cacheDuration' => 'P1D',  // 1 day
        'maxUploadFileSize' => 268435456, // 256MB
        'defaultImageQuality' => 100,
        'optimizeImageFilesize' => false, // this is key, it disables Imagicks 'smartresize' resulting in sharper images
        'upscaleImages' => false,
        'aliases' => array(
            'basePath' => BASEPATH . 'web/',
            'baseUrl'  => getenv('PRIMARY_SITE_URL'),
        ),
        'defaultSearchTermOptions' => array(
            'subLeft' => true,
            'subRight' => true,
        ),
        'siteUrl' => getenv('PRIMARY_SITE_URL')
    ],

    // Dev environment settings
    'dev' => [
        // Dev Mode (see https://craftcms.com/guides/what-dev-mode-does)
        'devMode' => true,

        // Prevent crawlers from indexing pages and following links
        'disallowRobots' => true,
    ],

    // Staging environment settings
    'staging' => [
        // Set this to `false` to prevent administrative changes from being made on Staging
        'allowAdminChanges' => true,

        // Don’t allow updates on Staging
        'allowUpdates' => false,

        // Prevent crawlers from indexing pages and following links
        'disallowRobots' => true,
    ],

    // Production environment settings
    'production' => [
        // Set this to `false` to prevent administrative changes from being made on Production
        'allowAdminChanges' => true,

        // Don’t allow updates on Production
        'allowUpdates' => false,
    ],
];
